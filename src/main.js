import Vue from 'vue'
import App from './App.vue'

Vue.directive('myon', {
  bind(el, binding) {
    const event = binding.arg;
    const fn = binding.value;
    console.log(event);
    if (event === 'mouseenter') {
      el.addEventListener(event, function() {
        el.style.backgroundColor = binding.value;
      })
    } else if (event === 'mouseleave') {
      el.addEventListener(event, function() {
        el.style.backgroundColor = null;
      })
    } else el.addEventListener(event, fn);

    
    // console.log(`clicked fired with expression: "${binding.expression}"`);
  },
})

new Vue({
  el: '#app',
  render: h => h(App)
})
